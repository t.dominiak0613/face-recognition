import os
from pathlib import Path

import cv2
from flask import flash, redirect, url_for
from model import Model


def show_webcam():
    camera = cv2.VideoCapture(0)
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            flash("Błąd! Brak kamery!")
            return redirect(url_for("auth.login"))
        else:

            ret, buffer = cv2.imencode(".jpg", frame)
            frame = buffer.tobytes()

            yield (
                b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + frame + b"\r\n"
            )  # concat frame one by one and show result


def compare_photo(email):
    camera = cv2.VideoCapture(0)
    frames = []
    frame_number = 0
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            flash("Błąd! Brak kamery!")
            return redirect(url_for("auth.login"))
        if frame_number % 5 == 0:
            frames.append(frame)
        elif frame_number > 50:
            break
        frame_number += 1

    model = Model()
    verification_result = model.verify(frames, email)
    return verification_result


def do_snapshots(email):
    camera = cv2.VideoCapture(0)
    frame_number = 0

    while True:
        frame_number += 1
        success, frame = camera.read()  # read the camera frame
        path = "images"
        Path(os.path.join(path, email)).mkdir(parents=True, exist_ok=True)

        if not success:
            flash("Błąd! Brak kamery!")
            return redirect(url_for("auth.signup"))
        else:
            if frame_number % 5 == 0:
                cv2.imwrite(
                    os.path.join(path, email + "/" + str(frame_number) + ".jpg"), frame
                )
                print(os.path.join(path, email + "/" + str(frame_number) + ".jpg"))
            ret, buffer = cv2.imencode(".jpg", frame)
            frame = buffer.tobytes()

            yield (
                b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + frame + b"\r\n"
            )  # concat frame one by one and show result
