from flask import Blueprint,Response,flash,redirect,render_template,request,url_for
from flask_login import login_required, login_user, logout_user
from werkzeug.security import check_password_hash, generate_password_hash
from __init__ import db
from models import User
from process_video import compare_photo, do_snapshots, show_webcam
from model import FaceDataset
from model import Model

auth = Blueprint("auth", __name__)


@auth.route("/login")
def login():
    return render_template("login.html")


@auth.route("/signup")
def signup():
    return render_template("signup.html")


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("main.index"))


@auth.route("/signup", methods=["POST"])
def signup_post():
    email = request.form.get("email")
    name = request.form.get("name")
    password = request.form.get("password")

    user = User.query.filter_by(
        email=email
    ).first()  # if this returns a user, then the email already exists in database

    if (
        user
    ):  # if a user is found, we want to redirect back to signup page so user can try again
        flash("Adres e-mail istnieje w bazie małpo!")
        return redirect(url_for("auth.signup"))

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(
        email=email,
        name=name,
        password=generate_password_hash(password, method="sha256"),
    )

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    return render_template("snapshots.html", name=new_user.email)


@auth.route("/login", methods=["POST"])
def login_post():
    email = request.form.get("email")
    password = request.form.get("password")
    remember = True if request.form.get("remember") else False
    user = User.query.filter_by(email=email).first()

    # check if the user actually exists
    # take the user-supplied password, hash it, and compare it to the hashed password in the database
    if not user or not check_password_hash(user.password, password):
        flash("Sprawdź swoje dane logowania i spróbuj ponownie.")
        return redirect(
            url_for("auth.login")
        )  # if the user doesn't exist or password is wrong, reload the page

    # verify photo
    verification = compare_photo(user.email)
    if not verification:
        flash(
            "Weryfikacja zdjęciowa nie powiodła się! Spróbuj ponownie. Po trzeciej nieudanej próbie poinformujemy PSY."
        )
        return redirect(url_for("auth.login"))  # if the user doesn't verified, reload

    # if the above check passes, then we know the user has the right credentials
    login_user(user, remember=remember)
    return redirect(url_for("main.profile"))


@auth.route("/verify")
def verify():
    return Response(show_webcam(), mimetype="multipart/x-mixed-replace; boundary=frame")


@auth.route("/snapshots/<name>")
def snapshots(name):
    return Response(
        do_snapshots(name), mimetype="multipart/x-mixed-replace; boundary=frame"
    )


@auth.route("/train/<name>")
def train(name):
    fd = FaceDataset(dataset_folder=f"images/{name}", one_person=True)
    fd.add_user_data_to_dataset()
    emb, labels = fd.embeddings, fd.labels
    model = Model()
    model.model_training(emb, labels)
    model.save_model()
    return redirect(url_for("auth.login"))
