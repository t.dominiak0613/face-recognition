"""training data set was downloaded from  http://vis-www.cs.umass.edu/lfw/#resources """

from typing import List
import pickle
import os
import tqdm
from collections import Counter
import numpy as np
import cv2
from face_identifier import FaceFinder

from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis

"""Processing a dataset. The classifier will not need images,
but a collection of embeddings with labels attached to them."""


class FaceDataset:

    def __init__(
            self,
            data_storage: str = "new_embeddings_with_labels.p",
            dataset_folder: str = ".face_recognition/dataset/lfw-deepfunneled",
            one_person: bool = False
    ) -> None:

        self.labels: List[str] = []
        self.embeddings: List[List[float]] = []
        self.data_storage = data_storage
        self.dataset_folder = dataset_folder
        self.one_person = one_person


    """Data_processing should allow declaring the minimum number of photos that will make a person added to the dataset,
    in case this number is not exceeded the person should have the label 'Unknown_Person'"""

    def data_processing(self, min_images_per_person: int = 200) -> None:

        ff = FaceFinder()

        persons = os.listdir(self.dataset_folder)
        for person in tqdm.tqdm(persons):
            if self.one_person:
                photos = persons
                path = self.dataset_folder + "/"
            else:
                photos = os.listdir(self.dataset_folder + "/" + person)
                path = self.dataset_folder + "/" + person + "/"

            for photo in photos:
                img = cv2.imread(path + photo)
                embeddings = ff.get_embeddings(img)
                if len(embeddings) == 128:
                    if len(photos) > min_images_per_person:
                        self.labels.append(person)
                    else:
                        self.labels.append("Unknown_Person")
                    self.embeddings.append(embeddings)
            if self.one_person:
                break

    def save_data(self) -> None:

        pickle.dump((self.embeddings, self.labels),
                    open(self.data_storage, "wb"))

    def load_data(self) -> None:

        self.embeddings, self.labels = pickle.load(
            open(self.data_storage, "rb"))

    def add_user_data_to_dataset(self) -> None:

        self.load_data()
        current_embeddings, current_labels = self.embeddings, self.labels
        self.data_processing()
        self.embeddings.extend(current_embeddings)
        self.labels.extend(current_labels)
        self.save_data()


class Model:

    def __init__(self, model_storage: str = "model.p") -> None:

        self.classifier = AdaBoostClassifier()
        self.model_storage = model_storage

    @staticmethod
    def model_comparision(embeddings: List[List[float]], labels: List[str]) -> None:

        classifiers = [
            KNeighborsClassifier(
                n_neighbors=3, algorithm="ball_tree", weights="distance"
            ),
            SVC(kernel="linear", C=0.025),
            SVC(gamma=2, C=1),
            DecisionTreeClassifier(max_depth=5),
            RandomForestClassifier(
                max_depth=5, n_estimators=10, max_features=1),
            MLPClassifier(alpha=1, max_iter=1000),
            AdaBoostClassifier(),
            GaussianNB(),
            QuadraticDiscriminantAnalysis(),
        ]
        for clf in classifiers:
            print(
                cross_val_score(
                    clf, X=embeddings, y=labels, cv=5, scoring="f1_weighted"
                )
            )

    def model_training(
            self, embeddings: List[List[float]], labels: List[str]
    ) -> AdaBoostClassifier:


        x_train, x_test, y_train, y_test = train_test_split(
            embeddings, labels, test_size=0.2, random_state=42, stratify=labels
        )
        self.classifier.fit(x_train, y_train)
        y_pred = self.classifier.predict(x_test)
        print(confusion_matrix(y_test, y_pred))
        return self.classifier

    def save_model(self) -> None:

        pickle.dump(self.classifier, open(self.model_storage, "wb"))

    def load_model(self) -> None:

        self.classifier = pickle.load(open(self.model_storage, "rb"))

    """integration of the model into the web application"""

    def verify(self, frames: List[np.ndarray], email: str) -> bool:
        self.load_model()
        ff = FaceFinder()
        predictions = []
        for frame in frames:
            embeddings = ff.get_embeddings(frame)
            if len(embeddings) == 128:
                embeddings = np.array(embeddings)
                embeddings = embeddings.reshape(1, -1)
                predictions.append(self.classifier.predict(embeddings)[0])

        most_common = Counter(predictions).most_common(1)
        if most_common[0][0] == email:
            return True
        else:
            return False

if __name__ == '__main__':

        model = Model()

        fd = FaceDataset(data_storage="new_embeddings_with_labels.p")
        fd.load_data()
        new_embeddings = [
            embedding
            for embedding, label in zip(fd.embeddings, fd.labels)
            if label != "Unknown_Person"
        ]
        new_labels = [
            label
            for embedding, label in zip(fd.embeddings, fd.labels)
            if label != "Unknown_Person"
        ]

        unknown = 0
        for embedding, label in zip(fd.embeddings, fd.labels):
            if label == "Unknown_Person":
                unknown += 1
                new_embeddings.append(embedding)
                new_labels.append(label)
                if unknown > 400:
                    break

        model.model_training(new_embeddings, new_labels)